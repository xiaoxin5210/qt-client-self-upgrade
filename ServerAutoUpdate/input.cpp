#include "input.h"
#include "iostream"

#include <QDebug>

input::input(QThread *parent) : QThread(parent)
{

}

void input::run()
{
    while (1) {
        int a;
        std::cin >> a;
        if (a == 1) {
            qDebug() << "是否发布更新";
        }
        a = 0;
        std::cin >> a;
        if (a == 1) {
             qDebug() << "开始发布更新";
             emit signalStartUpdate();
        }
    }
}
