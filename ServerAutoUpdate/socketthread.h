#ifndef SOCKETTHREAD_H
#define SOCKETTHREAD_H

#include <QObject>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QCryptographicHash>
#include <QJsonObject>
#include <QDir>

enum Type{
    UPDATE, //更新确认
    START, //文件开始
    DATA,
    FINISH,
    ERROR
};

struct FileData
{
    QString fileName;
    QString MD5;
    QByteArray fileContent;
    FileData()
        : fileName("")
        , MD5("")
        , fileContent("")
    {}
};

struct TcpData
{
    Type type;
    QByteArray data;
    FileData fileData;
};
struct name
{
    name() {}
};
class socketThread : public QObject
{
    Q_OBJECT
public:
    explicit socketThread(QTcpSocket* socket, int num);

private slots:
    void onStateChanged(QAbstractSocket::SocketState socketState);
    void onSendUpdateRequest(); //update begin
    void onReadyRead();
signals:
    void signalDisconnect(int num);
private:
    QByteArray envelop(TcpData data); //传输封包
    TcpData unpack(QByteArray data); //传输拆包
    void startJsonEnvelop(TcpData data); //数据封包
    void startJsonUnpack(QByteArray data); //数据开始拆包

    static const QString countMd5(const QString filePath); //

    const QStringList getDirAllFile(QString path);

    void startUpdate();
    void startTransfer();
    void finish(TcpData tData);
    void sendFinish();
private:
    QTcpSocket *mySocket;
    int num;
    QStringList fileList; //可用结构体来保存数据更好，保存每个文件的数据 避免重复获取
};

#endif // SOCKETTHREAD_H
